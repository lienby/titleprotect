// ==UserScript==
// @name         TitleProtect
// @version      0.1
// @namespace    https://github.com/SilicaAndPina/TitleProtect
// @description  Hide the title of every website
// @author       SilicaAndPina
// @match        *://*/*
// @grant        none
// ==/UserScript==




function changeFavicon(src) 
{
 var link = document.createElement('link'),
     oldLink = document.getElementById('dynamic-favicon');
 link.id = 'dynamic-favicon';
 link.rel = 'shortcut icon';
 link.href = src;
 if (oldLink) 
 {
  document.head.removeChild(oldLink);
 }
 document.head.appendChild(link);
}

function replaceTitle()
{
    document.title = "A Website";
    document.head = document.head || document.getElementsByTagName('head')[0];
    setTimeout(replaceTitle, 0);
    changeFavicon('http://www.iconarchive.com/download/i87099/graphicloads/colorful-long-shadow/Lock.ico');
}


(function() 
{
    'use strict';
    replaceTitle();

})();
